const MongooseError = require('mongoose/lib/error');

const makingValidationErrorFormat = (field, message) => {
  const err = new MongooseError.ValidationError('ObjectId');
  err.addError(field, { message });
  return err;
};

const makingDuplicateErrorFormat = () => {
  const err = new Error();
  err.name = 'MongoError';
  err.code = 11000;
  return err;
};

/**
 * Exception builder for http responses
 */
const exceptionBuilder = {
  /**
   * Making error response
   * @param {Object} err - error object
   */
  build(type, field, message) {
    if (type === 'Validation') {
      return makingValidationErrorFormat(field, message);
    }
    if (type === 'DuplicateKeyError') {
      return makingDuplicateErrorFormat();
    }
    return new Error();
  }
};

module.exports = exceptionBuilder;
