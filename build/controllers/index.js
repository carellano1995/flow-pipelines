module.exports = ({
    HttpStatus,
    Service,
    ErrorBuilder = { build: (_) => [501, 'not implemented'] },
    Logger,
}) => {
    return {
        getAll: async (req, res) => {
            const result = await Service.getAll();
            res.status(HttpStatus.OK).json(result);
        },
    };
};
