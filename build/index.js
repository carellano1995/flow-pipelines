const http = require('http');
const logger = require('./lib/logger');
const app = require('./server');

const defaultPort = 8000;

const server = http.createServer(app);
server.listen(process.env.PORT || defaultPort);
server.on('listening', function serve() {
    logger.info(this.address());
    logger.info(`Server listening on ${this.address().address}:${this.address().port}`);
});
