const winston = require('winston');
const database = require('./conf/database');
const appConfig = require('./conf/config');
const logger = require('./logger');

require('winston-mongodb');

const SECONDS_MONTH = 30 * 24 * 3600;

const spec = {
  onconfig: (config, next) => {
    const loggerConfig = config.get('logger');
    const databaseConfig = config.get('databaseConfig');
    const dbConfig = databaseConfig;
    const options = {
      level: loggerConfig.level,
      db: dbConfig.connectionUrl,
      collection: 'logs',
      expireAfterSeconds: dbConfig.expireAfterSeconds || 6 * SECONDS_MONTH,
      decolorize: false
    };

    logger.add(new winston.transports.MongoDB(options));
    appConfig.config(config);
    database.config(databaseConfig);

    next(null, config);
  }
};

module.exports = spec;
