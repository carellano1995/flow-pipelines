const HttpStatus = require('../lib/constants/http-status');
const ErrorBuilder = require('../lib/builders/error-builder');
const Logger = require('../lib/logger');

const FactoryController = require('../controllers/index');
const Service = require('../services/player');

module.exports = (router) => {
    const controller = FactoryController({
        HttpStatus,
        ErrorBuilder,
        Logger,
        Service,
    });

    router.get('/', controller.getAll);
};
