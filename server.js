const express = require("express");
const kraken = require("kraken-js");
const cors = require("cors");
const logger = require("./lib/logger");
const options = require("./lib/start-options.js");

const app = express();

app.use(cors());

app.use(kraken(options));

app.on("start", async () => {
    logger.info("Application ready to serve requests.");
    logger.info(`Environment: ${app.kraken.get("env:env")}`);
});

module.exports = app;
